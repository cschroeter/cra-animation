import React, { Component } from 'react';
import { Motion, TransitionMotion, spring } from 'react-motion';
import { Flex } from 'grid-styled';
import styled from 'styled-components';
import logo from './logo.svg';

const Box = styled.div`
  min-height: 300px;
  width: 100%;
  max-width: 100%;
`;

const Product = ({ details }) => (
  <Box style={{ overflow: 'hidden' }}>
    <img src={logo} alt="Logo" width="200" />
    <Motion style={{ x: spring(details ? 1 : 0) }}>
      {({ x }) =>
        <p style={{ opacity: x }}>This is detail text</p>
      }
    </Motion>
  </Box>
)

class App extends Component {

  constructor() {
    super();
    this.state = {
      items: [{ key: 'a', size: 1, details: false }, { key: 'b', size: 1, details: false }, { key: 'c', size: 1, details: false }, { key: 'd', size: 1, details: false }],
    }
  }

  showDetails() {
    this.setState({
      items: [{ key: 'b', size: 1 }]
    });
  }

  showAll() {
    this.setState({
      items: [{ key: 'a', size: 1, details: false }, { key: 'b', size: 1, details: false }, { key: 'c', size: 1, details: false }, { key: 'd', size: 1, details: false }],
    });
  }

  willLeave() {
    return { flexGrow: spring(0.00001), opacity: spring(0) }
  }

  didLeave(value) {
    this.setState({
      items: [{ key: 'b', size: 1, details: true }]
    });
  }

  willEnter(conf) {
    return { flexGrow: 0.00001, opacity: 0 };
  }

  render() {
    return (
      <div>
        <TransitionMotion
          willLeave={this.willLeave}
          willEnter={this.willEnter}
          didLeave={this.didLeave.bind(this)}
          styles={this.state.items.map(item => ({
            key: item.key,
            style: { flexGrow: spring(item.size), opacity: spring(1) },
            data: { details: item.details }
          }))}>
          {interpolatedStyles =>
            <Flex py={4}>
              {interpolatedStyles.map(config => {
                return <Flex width={[1, 1 / 2, 1 / 4]} flex="1" key={config.key} style={{ ...config.style, overflow: 'hidden' }}>
                  <Product details={config.data.details} />
                </Flex>
              })}
            </Flex>
          }
        </TransitionMotion>
        <button onClick={this.showDetails.bind(this)}>Details</button>
        <button onClick={this.showAll.bind(this)}>Show All</button>
      </div >
    );
  }
}

export default App;
